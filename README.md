# Assignment 10

All solutions must be placed on [CodeAnywhere](https://codeanywhere.com) into the folder `a10`.
Put each problem into the corresponding file `p1.py`, `p2.py`, `p3.py` *etc*.

All files must produce *no errors* when invoked with Python *e.g.* as
```
python p1.py
```

Each file must have a
```shell
#!/usr/bin/env python3
```
line at the very top, and must be made executable with a command
```shell
chmod a+x p1.py
```
(same for `p2.py` ...)




### Problem 1 (*100%*)

Write a function `valid_course(s)` which takes a string parameter `s`
and uses **only regular expressions** (that is, function `re.search()`) to determine
whether `s` is a valid name of a Computer Science course:
* it may have any number (including none) spaces in the beginning
* these spaces, if any, can only be followed by `'CSC-xxxx'`
* `'xxxx'` here denotes any sequence of digits (at least one)
* all of these may be followed by any number of spaces (just regular spaces)

To give an example,
```python
'      CSC-123456    '
'CSC-2345      '
'CSC-9     '
```
are all examples of valid course names, whereas
```python
'    a CSC-4573  b  '
```
or
```python
'   CSC 123565'
```
are **not** valid examples

The function must return `True` for a valid name, `False` for an invalid name
